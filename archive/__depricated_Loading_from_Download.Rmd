---
title: "Disease Analysis"
output: BiocStyle::html_document
---


Aim is to parse only the relevant disease data and generate a graph.



RNA-binding proteins and Diseases.


We focus here on human RBPs. 

Those will be analysed with OpenTargets data. 

Open Targets provides
 - disease association
    - target 
        - tractability (small molecule, antibody, etc)
    - disease
        - id
        - efo_info
    - association_score
    - is_direct: True or False
    - evidence_count
        - total
        - datatypes
        - datasources
 - target evidence
    - target
    - validated_against_schema_version
    - sourceID
    - unique_association_fields
    - drug
    - evidence
    - score 
    - type
    - id 
    



# Data preparation 

## Setup 
```{r}
suppressPackageStartupMessages({
    require(tidyverse)
    require(rjson)
    require(jsonlite)
    #require(jsonify)
    require(data.table)
})
```


## Input files

```{r}
input_dir <- "input"
```

### Setting up directories

```{r}
opentargets_dir <- file.path(input_dir, "opentargets")
opentargets_version <- "20.04"
omim_dir <- file.path(input_dir, "omim")
```


```{r}
dir.create(opentargets_dir, showWarnings = F)
dir.create(omim_dir, showWarnings = F)
```


```{r}
dir_processed <- file.path("input", "processed")
dir.create(dir_processed, showWarnings = F)
```


### Meta table

creating a table with all files and download info

```{r}
files <- tibble(
     db = "opentargets",
     file = "evidence", 
     file_path =  file.path(opentargets_dir, 
                            paste0(opentargets_version,
                                   "_evidence_data.json.gz")),
     download_link = paste0("https://storage.googleapis.com/open-targets-data-releases/", opentargets_version, "/output/", opentargets_version, "_evidence_data.json.gz")) %>%
  add_row(
     db = "opentargets",
     file = "target", 
     file_path =  file.path(opentargets_dir, 
                            paste0(opentargets_version,
                                   "_target_list.csv.gz")),
     download_link = paste0("https://storage.googleapis.com/open-targets-data-releases/", opentargets_version, "/output/", opentargets_version, "_target_list.csv.gz")) %>%
  add_row(
     db = "opentargets",
     file = "disease", 
     file_path =  file.path(opentargets_dir, 
                            paste0(opentargets_version,
                                   "_disease_list.csv.gz")),
     download_link = paste0("https://storage.googleapis.com/open-targets-data-releases/", opentargets_version, "/output/", opentargets_version, "_disease_list.csv.gz")) %>%
    add_row(
     db = "opentargets",
     file = "diseasejson", 
     file_path =  file.path(opentargets_dir, 
                            paste0(opentargets_version,
                                   "_disease_list.json.gz")),
     download_link = paste0("https://storage.googleapis.com/open-targets-data-releases/", opentargets_version, "/output/", opentargets_version, "_disease_list.json.gz")) %>%
  add_row(
     db            = "opentargets",
     file          = "association", 
     file_path     =  file.path(opentargets_dir, 
                            paste0(opentargets_version,
                                   "_association_data.json.gz")),
     download_link = paste0("https://storage.googleapis.com/open-targets-data-releases/", opentargets_version, "/output/", opentargets_version, "_association_data.json.gz")) %>%
  add_row(
     db            = "omim", 
     file          = "mim2gene",
     file_path     = file.path(omim_dir, "mim2gene.txt"),
     download_link = "https://omim.org/static/omim/data/mim2gene.txt") %>%
  
  add_row(
     db            = "omim", 
     file          = "mimTitles",
     file_path     = file.path(omim_dir, "mimTitles.txt"),
     download_link = "https://data.omim.org/downloads/K82Krc9oTiC6ybxNiqUCNQ/mimTitles.txt") %>%
  
  add_row(
     db            = "omim", 
     file          = "genemap2",
     file_path     = file.path(omim_dir, "genemap2.txt"),
     download_link = "https://data.omim.org/downloads/K82Krc9oTiC6ybxNiqUCNQ/genemap2.txt") %>%
  
  add_row(
     db            = "omim", 
     file          = "morbidmap",
     file_path     = file.path(omim_dir, "morbidmap.txt"),
     download_link = "https://data.omim.org/downloads/K82Krc9oTiC6ybxNiqUCNQ/morbidmap.txt")

```


### Mirroring the data

```{r}
apply(files, 1, function(x) {
   if(!file.exists(x[["file_path"]])) {
          download.file(url = x[["download_link"]],
                 destfile = x[["file_path"]])
   }
}) %>% invisible()
```

### Unpacking the data

```{r, eval = FALSE}
#TODO
if(!file.exists(opentargets_files[["association"]])) {
    download.file("", paste0(opentargets_files[["association"]], ".gz"))
    system(paste0("gunzip ", opentargets_files[["association"]], ".gz"))
}

if(!file.exists(opentargets_files[["evidence"]])) {
    download.file("", paste0(opentargets_files[["evidence"]], ".gz"))
    system(paste0("gunzip ", opentargets_files[["evidence"]], ".gz"))
}

```

### Reading in the data


#### OMIM 

##### MIM2gene

mim2gene, mapping from MIM to gene

```{r}
mim2gene <- read_tsv(
  file = files %>% 
    filter(file == "mim2gene") %>%
    pull(file_path),
  col_names = c("MIM_Number",
                "MIM_Entry_Type",
                "Entrez_Gene_ID",
                "Approved_Gene_Symbol",
                "Ensembl_Gene_ID"),
  comment = "#",
  col_types = "dcdcc"
  )
```


```{r}
saveRDS(object = mim2gene, 
        file = file.path(dir_processed, "mim2gene.Rds"))
```

##### MimTitles

mimTitles: description of the MIM

```{r}
mimTitles <- read_tsv(
  file = files %>% 
    filter(file == "mimTitles") %>%
    pull(file_path),
  col_names = c("Prefix",
                "MIM_Number",
                "Preferred_Title",
                "Alternative_Title",
                "Symbols_included_Title"),
  comment = "#",
  col_types = "cdccc"
  )
```

```{r}
saveRDS(object = mimTitles, 
        file = file.path(dir_processed, "mimTitles.Rds"))
```



##### Genemap2 

genemap2: 

```{r}
genemap2 <- read_tsv(
  file = files %>% 
    filter(file == "genemap2") %>%
           pull(file_path),
    col_names = c("Chromosome",
                  "Genomic_Position_Start",
                  "Genomic_Position_End",
                  "Cyto_Location",
                  "Computed_Cyto_Location",
                  "MIM_Number",
                  "Gene_Symbols",
                  "Gene_Name",
                  "Approved_Symbol",
                  "Entrez_Gene_ID",
                  "Ensembl_Gene_ID",
                  "Comments",
                  "Phenotypes",
                  "Mouse_Gene_Symbol_ID"),
    col_types = "cddccdcccdcccc",
    comment = "#"
  )
```

```{r}
saveRDS(object = genemap2, 
        file = file.path(dir_processed, "genemap2.Rds"))
```

##### Morbidmap

morbidmap

```{r}
morbidmap <- read_tsv(
  file = files %>% 
    filter(file == "morbidmap") %>%
           pull(file_path),
    col_names = c("Phenotype",
                  "Gene_Symbols",
                  "MIM_Number",
                  "Cyto Location"),
    col_types = "ccdc",
    comment = "#"
  )
```


```{r}
saveRDS(object = morbidmap, 
        file = file.path(dir_processed, "morbidmap.Rds"))
```

#### Open Targets  

##### Target list 

This table describes the targets and identifier relationships 

```{r}
target_list <- read_csv(files %>% filter(file == "target") %>% pull(file_path),
                        col_names = c("ensembl_id",
                                      "hgnc_approved_symbol",
                                      "uniprot_accessions",
                                      "number_of_associations"),
                        skip = 1,
                        col_types = "cccd")
target_list
```

##### Disease List

This table describes all diseases and associated IDs. 

Sample the first lines of the file

```{r}
readLines(files %>% filter(file == "disease") %>% pull(file_path), n = 3)
```

```{r}
disease_list <- read_csv(files %>% filter(file == "disease") %>% pull(file_path),
                        col_names = c("efo_id",
                                      "disease_full_name",
                                      "number_of_associations"),
                        col_types = "ccd",
                        skip = 1)

disease_list
```

##### Evidence

```{r}
readLines(files %>% filter(file == "evidence") %>% pull(file_path), n = 1) %>% gsub(pattern = "\\\"", replacement = "'")
```

Formated with https://jsonformatter.curiousconcept.com/

```{json, eval = FALSE}
{
   "target":{
      "id":"ENSG00000154358",
      "gene_info":{
         "symbol":"OBSCN",
         "name":"obscurin, cytoskeletal calmodulin and titin-interacting RhoGEF",
         "geneid":"ENSG00000154358"
      },
      "target_type":"gene_evidence",
      "activity":"predicted_damaging"
   },
   "validated_against_schema_version":"1.2.8",
   "sourceID":"phenodigm",
   "disease":{
      "efo_info":{
         "therapeutic_area":{
            "labels":[
               "musculoskeletal or connective tissue disease",
               "genetic, familial or congenital disease",
               "nervous system disease"
            ],
            "codes":[
               "OTAR_0000006",
               "OTAR_0000018",
               "EFO_0000618"
            ]
         },
         "path":[
            [
               "OTAR_0000006",
               "EFO_0009676",
               "EFO_0002970",
               "Orphanet_183497",
               "Orphanet_207049",
               "Orphanet_209224",
               "Orphanet_268129"
            ],
            [
               "OTAR_0000006",
               "EFO_0009676",
               "EFO_0002970",
               "Orphanet_183497",
               "Orphanet_206634",
               "Orphanet_206656",
               "Orphanet_593",
               "Orphanet_268129"
            ],
            [
               "OTAR_0000006",
               "EFO_0009676",
               "EFO_0002970",
               "MONDO_0003939",
               "Orphanet_206634",
               "Orphanet_206656",
               "Orphanet_593",
               "Orphanet_268129"
            ],
            [
               "OTAR_0000006",
               "EFO_0009676",
               "EFO_0002970",
               "MONDO_0003939",
               "EFO_0004145",
               "Orphanet_206656",
               "Orphanet_593",
               "Orphanet_268129"
            ],
            [
               "OTAR_0000006",
               "EFO_0009676",
               "EFO_0002970",
               "MONDO_0003939",
               "EFO_0004145",
               "MONDO_0002921",
               "Orphanet_593",
               "Orphanet_268129"
            ],
            [
               "OTAR_0000018",
               "EFO_0000508",
               "Orphanet_71859",
               "Orphanet_183497",
               "Orphanet_207049",
               "Orphanet_209224",
               "Orphanet_268129"
            ],
            [
               "OTAR_0000018",
               "EFO_0000508",
               "Orphanet_71859",
               "Orphanet_183497",
               "Orphanet_206634",
               "Orphanet_206656",
               "Orphanet_593",
               "Orphanet_268129"
            ],
            [
               "OTAR_0000018",
               "EFO_0000508",
               "Orphanet_71859",
               "Orphanet_98497",
               "Orphanet_206634",
               "Orphanet_206656",
               "Orphanet_593",
               "Orphanet_268129"
            ],
            [
               "EFO_0000618",
               "EFO_0009387",
               "EFO_0003100",
               "EFO_1001902",
               "Orphanet_206634",
               "Orphanet_206656",
               "Orphanet_593",
               "Orphanet_268129"
            ],
            [
               "EFO_0000618",
               "EFO_0009387",
               "EFO_0003100",
               "Orphanet_98497",
               "Orphanet_206634",
               "Orphanet_206656",
               "Orphanet_593",
               "Orphanet_268129"
            ],
            [
               "EFO_0000618",
               "EFO_0004149",
               "EFO_0003100",
               "EFO_1001902",
               "Orphanet_206634",
               "Orphanet_206656",
               "Orphanet_593",
               "Orphanet_268129"
            ],
            [
               "EFO_0000618",
               "EFO_0004149",
               "EFO_0003100",
               "Orphanet_98497",
               "Orphanet_206634",
               "Orphanet_206656",
               "Orphanet_593",
               "Orphanet_268129"
            ],
            [
               "EFO_0000618",
               "Orphanet_71859",
               "Orphanet_183497",
               "Orphanet_207049",
               "Orphanet_209224",
               "Orphanet_268129"
            ],
            [
               "EFO_0000618",
               "Orphanet_71859",
               "Orphanet_183497",
               "Orphanet_206634",
               "Orphanet_206656",
               "Orphanet_593",
               "Orphanet_268129"
            ],
            [
               "EFO_0000618",
               "Orphanet_71859",
               "Orphanet_98497",
               "Orphanet_206634",
               "Orphanet_206656",
               "Orphanet_593",
               "Orphanet_268129"
            ]
         ],
         "efo_id":"http://www.orpha.net/ORDO/Orphanet_268129",
         "label":"Spheroid body myopathy"
      },
      "source_name":"Myopathy, Spheroid Body",
      "name":"Spheroid body myopathy",
      "id":"Orphanet_268129"
   },
   "unique_association_fields":{
      "predictionModel":"mgi_predicted",
      "disease_phenodigm_id":"OMIM:182920",
      "model_genetic_background":"involves: 129S1/Sv * 129X1/SvJ",
      "target_id":"http://identifiers.org/ensembl/ENSG00000154358",
      "model_gene_id":"http://identifiers.org/ensembl/ENSMUSG00000061462",
      "disease_id":"http://www.orpha.net/ORDO/Orphanet_268129",
      "score":"1.000000000000000",
      "datasource":"phenodigm",
      "model_description":"Obscn<tm1Chen>/Obscn<tm1Chen>"
   },
   "evidence":{
      "orthologs":{
         "human_gene_id":"http://identifiers.org/ensembl/ENSG00000154358",
         "model_gene_id":"http://identifiers.org/ensembl/ENSMUSG00000061462",
         "resource_score":{
            "type":"probability",
            "method":{
               "description":"orthology from MGI"
            },
            "value":1.0
         },
         "provenance_type":{
            "database":{
               "version":"2016",
               "id":"MGI"
            }
         },
         "is_associated":false,
         "species":"mouse",
         "evidence_codes":[
            "http://identifiers.org/eco/ECO:0000265"
         ],
         "date_asserted":"2020-03-30T17:09:46.477494"
      },
      "evidence_codes_info":[
         [
            {
               "eco_id":"ECO_0000179",
               "label":"animal model system study evidence"
            }
         ]
      ],
      "biological_model":{
         "model_id":"MGI:4361274",
         "allele_ids":"",
         "zygosity":"oth",
         "phenotypes":[
            {
               "term_id":"http://purl.obolibrary.org/obo/MP_0009404",
               "label":"centrally nucleated skeletal muscle fibers",
               "species":"mouse",
               "id":"MP:0009404"
            },
            {
               "term_id":"http://purl.obolibrary.org/obo/MP_0004088",
               "label":"abnormal sarcoplasmic reticulum morphology",
               "species":"mouse",
               "id":"MP:0004088"
            },
            {
               "term_id":"http://purl.obolibrary.org/obo/MP_0000751",
               "label":"myopathy",
               "species":"mouse",
               "id":"MP:0000751"
            }
         ],
         "genetic_background":"involves: 129S1/Sv * 129X1/SvJ",
         "resource_score":{
            "type":"probability",
            "method":{
               "description":""
            },
            "value":1.0
         },
         "allelic_composition":"Obscn<tm1Chen>/Obscn<tm1Chen>",
         "model_gene_id":"http://identifiers.org/ensembl/ENSMUSG00000061462",
         "provenance_type":{
            "database":{
               "version":"2017",
               "id":"MGI"
            }
         },
         "is_associated":false,
         "species":"mouse",
         "evidence_codes":[
            "http://identifiers.org/eco/ECO:0000179"
         ],
         "date_asserted":"2020-03-30T17:09:46.477494"
      },
      "evidence_codes":[
         "ECO_0000179"
      ],
      "disease_model_association":{
         "human_phenotypes":[
            {
               "term_id":"http://purl.obolibrary.org/obo/HP_0003198",
               "label":"Myopathy",
               "species":"human",
               "id":"HP:0003198"
            }
         ],
         "model_phenotypes":[
            {
               "term_id":"http://purl.obolibrary.org/obo/MP_0009404",
               "label":"centrally nucleated skeletal muscle fibers",
               "species":"mouse",
               "id":"MP:0009404"
            },
            {
               "term_id":"http://purl.obolibrary.org/obo/MP_0004088",
               "label":"abnormal sarcoplasmic reticulum morphology",
               "species":"mouse",
               "id":"MP:0004088"
            },
            {
               "term_id":"http://purl.obolibrary.org/obo/MP_0000751",
               "label":"myopathy",
               "species":"mouse",
               "id":"MP:0000751"
            }
         ],
         "model_id":"MGI:4361274",
         "disease_id":"http://www.orpha.net/ORDO/Orphanet_268129",
         "provenance_type":{
            "database":{
               "version":"June 2016",
               "id":"PhenoDigm"
            }
         },
         "is_associated":false,
         "resource_score":{
            "type":"summed_total",
            "method":{
               "description":"phenodigm_model_to_disease_score"
            },
            "value":1.0
         },
         "evidence_codes":[
            "http://identifiers.org/eco/ECO:0000057"
         ],
         "date_asserted":"2020-03-30T17:09:46.477494"
      }
   },
   "access_level":"public",
   "scores":{
      "association_score":1.0
   },
   "type":"animal_model",
   "id":"ffffe6d45cb0d9d28100c0b9cc694657"
}

```


```{r}
##evidence <- fromJSON(file = opentargets_files[["evidence"]])
evidence_stream <- files %>% 
                             filter(file == "evidence") %>%
                             pull(file_path) %>% 
                             pipe %>% 
                             gzcon %>% stream_in

evidence <- fromJSON(file = "evidence_test.json",
                     simplify = TRUE)

stream_in(gzcon())


evidence
```

##### Association Test

```{r}
association_stream <- files %>% 
                             filter(file == "association") %>%
                             pull(file_path) %>% 
                             pipe %>% 
                             gzcon %>% stream_in

#association <- fromJSON(file = opentargets_files[["association"]])
association <- fromJSON(file = "nov19/asstest100.json", simplify = TRUE)

str(association)
```

```{r}
X <- as.data.frame(association, stringsAsFactors = F) %>% as_tibble

write_tsv(X, path = "ass100.tsv")
```


```{r}
association <- jsonlite::fromJSON(txt = "nov19/asstest3.json")#,
                              #    simplifyVector = TRUE,
                               #   flatten = TRUE)

```


